class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :state
      t.string :result
      t.integer :timestampstarted
      t.integer :timestampcompleted
      t.integer :command_id
      t.integer :server_id
      t.string :args

      t.timestamps
    end
  end
end
