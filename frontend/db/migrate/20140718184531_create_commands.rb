class CreateCommands < ActiveRecord::Migration
  def change
    create_table :commands do |t|
      t.string :name
      t.string :description
      t.string :command
      t.string :args

      t.timestamps
    end
  end
end
