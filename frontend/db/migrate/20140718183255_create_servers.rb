class CreateServers < ActiveRecord::Migration
  def change
    create_table :servers do |t|
      t.string :shortname
      t.string :longname
      t.string :localip
      t.string :wanip
      t.text :description

      t.timestamps
    end
  end
end
