Rails.application.routes.draw do
  get 'jobrequests/index'

  get  'api/jobrequest'
  post 'api/jobrequest'

  get  'api/index'
  post 'api/index'

  get  '/login' => 'login#index'
  post '/login' => 'login#index'

  get  '/logout' => 'login#logout'

  get  '/jobrequests/:id/approve', to: 'jobrequests#approve', as: 'jobrequest_approve'
  get  '/jobrequests/:id/disapprove', to: 'jobrequests#disapprove', as: 'jobrequest_disapprove'
  post '/jobrequests/:id/disapprove', to: 'jobrequests#disapprove', as: 'jobrequest_disapprove_post'

  resources :commands, :servers, :jobs, :jobrequests

  root to: 'login#index'

end
