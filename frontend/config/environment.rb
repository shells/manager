# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

# Mail stuff
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
	:address              => '10.0.1.102',
	:port                 => '25',
	:domain               => 'insomnia247.nl',
	:authentication       => 'none',
	:enable_starttls_auto => false
}

# Show time just the way we like it
def showtime(timestamp)
	timestamp ? Time.at(timestamp).strftime('%F %T') : "-"
end

def loadconfig()
	configfile = 'config/rabbitmq.conf'
	if(File.exists?(configfile))
		configdata = ''
		File.open(configfile) do |file|
			file.each do |line|
				configdata << line
			end
		end

		config = JSON.parse(configdata)
	else
		raise "Config file '#{configfile}' does not exist."
	end

	return config
end
