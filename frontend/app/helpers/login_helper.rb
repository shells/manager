module LoginHelper
	require 'bcrypt'
	def checkpasword( pass )
		config = loadconfig()
		return BCrypt::Password.new(config['admin_hash']) == pass
	end

	def checkauth()
		if session[:authed]
			return true
		else
			flash[:error] = "Need to be logged in to view this page"
			redirect_to '/login'
			return false
		end
	end
end
