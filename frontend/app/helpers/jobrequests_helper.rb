module JobrequestsHelper
	# List of jobs that do not require admin approval
	# Values of key-value pair are for future use

	def autorun(j)
		autorun_jobs = {
			'httpd_public_html_enable'  => '',
			'httpd_public_html_disable' => '',
			'httpd_cgi-bin_enable'      => '',
			'httpd_chi-bin_disable'     => '',
			'httpd_logs_enable'         => '',
			'httpd_logs_disable'        => '',
			'nanobot_setnick'           => '',
			'twofactor_google_enable'   => '',
			'twofactor_google_disable'  => ''
		}

		return autorun_jobs.has_key? j
	end

	def check_key(key)
		config = loadconfig()
		return config['api_pass'] == key
	end

	def update_job(j)
		if !check_key(j[:key])
			return "Invalid key"
		end

		# Get job
		job = Job.find(j[:id])

		# Update values
		job.timestampcompleted = j[:timestamp]
		job.state              = j[:exitcode]
		job.result             = j[:message]
		job.save
	end
end
