module JobsHelper
	# Function to submit jobs to the queue
	def queue_job(job)

		# Load configuration
		config = loadconfig()
		
		# Read encryption key
		private_key = OpenSSL::PKey::RSA.new(File.read(config['private_keyfile']), config['encryption_pass'])

		# Establish connection to RabbitMQ
		conn = Bunny.new(
			:host     => config['host'],
			:user     => config['user'],
			:password => config['pass'],
		)
		conn.start

		ch = conn.create_channel
		q  = ch.queue(job.server.shortname)

		# Build message
		message = JSON.generate({
			:id        => job.id,
			:timestamp => job.timestampstarted,
			:command   => job.command.command,
			:args      => job.args
		})

		# Encrypt message
		if(message.length > 501)
			raise "String too long to encrypt."
		end

		string = private_key.private_encrypt(message)

		# Send message to the queue
		ch.default_exchange.publish(string, :routing_key => q.name)

		conn.close
	end
end
