class JobwaitingMailer < ActionMailer::Base
	default from: 'no-reply@insomnia247.nl'

	def jobwaiting(jobrequest, comments)
		@jobrequest = jobrequest
		@comments   = comments
		mail :to => 'coolfire@insomnia247.nl', :subject => "New pending job for #{@jobrequest.user}"
	end

	def jobpending(jobrequest)
		@jobrequest = jobrequest
		job     = Command.find(jobrequest.command_id)
		mail :to => jobrequest.email, :subject => "#{job.name} pending"
	end

	def jobdisapproved(jobrequest, reason)
		@reason = reason
		job     = Command.find(jobrequest.command_id)
		mail :to => jobrequest.email, :subject => "#{job.name} disapproved"
	end
end
