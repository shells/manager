class LoginController < ApplicationController
	include LoginHelper

	def index
		# Check if parameters are set
		if params[:login_form]
			if params[:login_form][:username] && params[:login_form][:password]
				# Check if username and password are correct
				if params[:login_form][:username] =~ /admin/i && checkpasword( params[:login_form][:password] )

					session[:authed] = true
					flash[:notice] = "Login successful"
					redirect_to jobs_path
				else
					flash[:error] = "Username or password incorrect"
				end
			end
		end
	end

	def logout
		session[:authed] = false
		session.delete(:authed)
		flash[:notice] = "Logged out"
		redirect_to '/login'
	end
end
