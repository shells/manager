class ServersController < ApplicationController
	include LoginHelper

	before_action :checkauth
	
	def index
		@servers = Server.all
	end

	def new
	end

	def create
		@server = Server.new(server_params)
		@server.save
		redirect_to @server
	end

	def show
		begin
			@server = Server.find(params[:id])
		rescue => e
			flash[:error] = "No server with that ID."
			redirect_to servers_path
		end

		begin
			@jobcount = @server.jobs.size
		rescue => e
			@jobcount = 0
		end

		begin
			@jobrequestcount = @server.jobrequests.size
		rescue => e
			@jobrequstcount = 0
		end

		begin
			if @jobcount > 0
				@jobs = @server.jobs.order(id: :desc).page(params[:page])
			end
		rescue => e
			flash[:error] = "Could not retrieve server jobs."
			redirect_to servers_path
		end
		
	end

	private
	def server_params
		params.require(:new_server).permit(:shortname, :longname, :localip, :wanip, :description)
	end
end
