class JobrequestsController < ApplicationController
	include JobsHelper
	include LoginHelper

	before_action :checkauth
	
	def index
		@jobrequests = Jobrequest.all
	end

	def approve
		jr = Jobrequest.find(params[:id])

		# Make job out of request
		@job = Job.new()
		@job.server_id        = jr.server_id
		@job.command_id       = jr.command_id
		@job.args             = "#{jr.user} #{jr.email} #{jr.args}"
		@job.state            = 2
		@job.result           = 'Waiting for results'
		@job.timestampstarted = DateTime.now
		@job.save

		# Update state of request
		jr.state              = 0
		jr.result             = 'Approved'
		jr.timestampcompleted = DateTime.now
		jr.save

		# Fire job into rabbitmq
		queue_job(@job)

		redirect_to @job
	end

	def disapprove
		if params[:disapprove_reason]
			# Update request
			jr = Jobrequest.find(params[:id])
			jr.state  = 1
			jr.result = "Disapproved: #{params[:disapprove_reason][:reason]}"
			jr.timestampcompleted = DateTime.now
			jr.save

			# Send email explaining disapproval
			JobwaitingMailer.jobdisapproved(jr, params[:disapprove_reason][:reason]).deliver

			redirect_to jr
		end
	end

	def show
		begin
			@job = Jobrequest.find(params[:id])
		rescue => e
			flash[:error] = 'No jobrequest with that ID'
			redirect_to jobrequests_path
		end
	end
end
