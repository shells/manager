class JobsController < ApplicationController
	include JobsHelper
	include LoginHelper

	before_action :checkauth
	
	def index
		@jobs = Job.all
	end

	def new
		# Check if parameters are set
		begin
			if params[:server_id]
				@server = Server.find(params[:server_id])
			else
				@server = Server.find(1)
			end
		rescue => e
			flash[:error] = "Need to create a server before you can create a job."
			redirect_to new_server_path
			return
		end	

		begin
			if params[:command_id]
				@command = Command.find(params[:command_id])
			else
				@command = Command.find(1)
			end
		rescue => e
			flash[:error] = "Need to create a command before you can create a job."
			redirect_to new_command_path
			return
		end

		@servers = Server.all
		@commands = Command.order('name ASC').all
	end

	def create
		@job = Job.new()
		@job.server_id        = params[:server_id][:server_id]
		@job.command_id       = params[:command_id][:command_id]
		@job.args             = params[:new_job][:args]
		@job.state            = 2
		@job.result           = "Waiting for results"
		@job.timestampstarted = DateTime.now
		@job.save

		# Fire job into rabbitmq
		queue_job(@job)

		redirect_to @job
	end

	def show
		begin
			@job = Job.find(params[:id])
		rescue => e
			flash[:error] = "No job with that ID."
			redirect_to jobs_path
		end
	end
end
