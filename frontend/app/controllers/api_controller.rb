class ApiController < ApplicationController
	include JobsHelper
	include JobrequestsHelper

	skip_before_filter :verify_authenticity_token, only: [:index, :jobrequest]

	def index
		@result = update_job(params)
	end

	def jobrequest
		@result = ""

		if !check_key(params[:key])
			@result = "Invalid key"
			return @result
		end

		# Check if this needs to be a request or if it can be done directly
		if(autorun(params[:command_id]))
			@job = Job.new()
			@job.server_id        = Server.find_by_shortname(params[:server_id]).id
			@job.command_id       = Command.find_by_command(params[:command_id]).id
			@job.args             = "#{params[:user]} #{params[:email]} #{params[:args]}"
			@job.state            = 2
			@job.result           = "Waiting for results"
			@job.timestampstarted = DateTime.now
			@job.save

			# Fire job into rabbitmq
			queue_job(@job)
			@result = "Job is being executed"
		else
			# Make jobrequest
			@jobr = Jobrequest.new()
			@jobr.server_id        = Server.find_by_shortname(params[:server_id]).id
			@jobr.command_id       = Command.find_by_command(params[:command_id]).id
			@jobr.args             = params[:args]
			@jobr.state            = 2
			@jobr.result           = "Waiting for approval"
			@jobr.timestampstarted = DateTime.now
			@jobr.user             = params[:user]
			@jobr.email            = params[:email]
			@jobr.save

			# Send notification to admin there is a pending job
			JobwaitingMailer.jobwaiting(@jobr, params[:comments]).deliver

			# Send notification to user that the job is received and pending approval
			JobwaitingMailer.jobpending(@jobr).deliver

			@result = "Job is pending approval"
		end
	end
end
