class CommandsController < ApplicationController
	include LoginHelper

	before_action :checkauth
	
	def new
	end

	def create
		@command = Command.new(command_params)
		@command.save
		redirect_to @command
	end

	def show
		begin
			@command = Command.find(params[:id])
		rescue => e
			flash[:error] = "No command with that ID."
			redirect_to commands_path
		end

		begin
			@commandcount = @command.jobs.size
		rescue => e
			@commandcount = 0			
		end
	end

	private
	def command_params
		params.require(:new_command).permit(:name, :description, :command, :args)
	end
end
