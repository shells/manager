#!/usr/bin/env ruby
# encoding: utf-8

require 'syslog'
require 'bunny'
require 'json'
require 'optparse'
require 'openssl'
require 'time'
require 'net/http'
require 'uri'
require 'thread'

# Function to report errors (to syslog or to stderr)
def report(message)
    if(@daemonize)
        Syslog.log(Syslog::LOG_WARNING, message)
    else
        warn(message)
    end
end

# Variable declarations
@daemonize = false
options = {}
config = {}

# Parse commandline options
OptionParser.new do |opts|
    # Alternate configuration file
    opts.on('-c file', '--config file', String, 'Specify config file. (Default: config/rabbitmq.conf') do |o|
        options[:config] = o
    end

    # Daemonize option
    opts.on('-d', '--daemonize', 'Run in background') do |o|
        options[:daemon] = o
    end
end.parse!

# Default config file if no alternate is specified
if(options[:config].nil?)
    options[:config] = 'config/rabbitmq.conf'
end

# Read config
if(File.exists?(options[:config]))
    configdata = ''
    File.open(options[:config]) do |file|
        file.each do |line|
            configdata << line
        end
    end

    # Parse config
    config = JSON.parse(configdata)
else
    raise "Config file does not exist: #{options[:config]}"
end

# Check if we should be daemonized
if(options[:daemon] || config['daemonize'])
        puts "Worker running in background."
        @daemonize = true
        Process.daemon

        # Start syslog
        Syslog.open("result_listener", Syslog::LOG_PID, Syslog::LOG_DAEMON | Syslog::LOG_LOCAL3)
    end

def results(options, config)
    # Start up RabbitMQ
    conn = Bunny.new(
                        :host     => config['host'],
                        :user     => config['user'],
                        :password => config['pass'],
                    )
    conn.start

    ch   = conn.create_channel
    q    = ch.queue(config['channel_results'])


    # Read private key so we can decrypt messages
    private_key = OpenSSL::PKey::RSA.new(File.read(config['private_keyfile']), config['encryption_pass'])


    # Start waiting for messages
    q.subscribe(:block => true) do |delivery_info, properties, payload|

        # Try to decrypt and parse message
        begin
            message = JSON.parse(private_key.private_decrypt(payload))

            # Check if the message seems to be authentic and sane
            # and attempt to execute it.
            if(Time.now.to_i > message['timestamp'] + config['max_skew'])
                report("Rejected message #{message['id']}: Timestamp too old. (#{delivery_info[:channel].connection.host})")
            else
                uri = URI.parse(config['api_url_results'])
                response = Net::HTTP.post_form(uri,
                    {
                        'id'        => message['id'],
                        'timestamp' => message['timestamp'],
                        'exitcode'  => message['exitcode'],
                        'message'   => message['message'],
                        'key'       => config['api_pass'],
                    }
                )
            end
        rescue Exception => e
            report("Could not decode message. (#{delivery_info[:channel].connection.host})")
        end
    end

    conn.close
end

def requests(options, config)
    # Start up RabbitMQ
    conn = Bunny.new(
                        :host     => config['host'],
                        :user     => config['user'],
                        :password => config['pass'],
                    )
    conn.start

    ch   = conn.create_channel
    q    = ch.queue(config['channel_requests'])


    # Read private key so we can decrypt messages
    private_key = OpenSSL::PKey::RSA.new(File.read(config['private_keyfile']), config['encryption_pass'])

    # Start waiting for messages
    q.subscribe(:block => true) do |delivery_info, properties, payload|

        # Try to decrypt and parse message
        begin
            message = JSON.parse(private_key.private_decrypt(payload))

            # Check if the message seems to be authentic and sane
            # and attempt to execute it.
            if(Time.now.to_i > message['timestamp'] + config['max_skew'])
                report("Rejected message #{message['id']}: Timestamp too old. (#{delivery_info[:channel].connection.host})")
            else
                uri = URI.parse(config['api_url_requests'])
                response = Net::HTTP.post_form(uri,
                    {
                        'server_id'  => message['server'],
                        'command_id' => message['command'],
                        'args'       => message['args'],
                        'user'       => message['user'],
                        'email'      => message['email'],
                        'key'        => config['api_pass'],
                        'comments'  => message['comments'],
                    }
                )
            end
        rescue Exception => e
            report("Could not decode message. (#{delivery_info[:channel].connection.host})")
        end
    end

    conn.close
end

res = Thread.new { results( options, config ) }
req = Thread.new { requests( options, config ) }

res.join
req.join